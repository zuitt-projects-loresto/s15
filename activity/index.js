// oddEvenChecker

function oddEvenChecker (num){
	
	if(typeof num === "number") {
		if(num % 2 == 0){
			console.log("The number is even.");

		} else{
			console.log("The number is odd.");

		}
	} else if(typeof num === "string"){
		console.log("Invalid Input");
	}
};

//budgetChecker

function budgetChecker(budget) {
	
	if(typeof budget === "number") {
		if(budget > 40000){
			console.log("You are over budget.");

		} else {
			console.log("You have resources left.");

		}
	} else{
		console.log("Invalid Input.");
	}
};